package com.antoniotari.reactiveampacheapp.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;

import java.lang.ref.WeakReference;

import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by antoniotari on 2017-03-13.
 */

public class FastScrollWrapper extends OnScrollListener {

    public interface IFastScrollWrapper {
        int getTotalItems();
    }

    private static final int TIMEOUT_SCROLL_BAR = 2000;
    private static final int MIN_ITEMS_SHOW = 24;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private final WeakReference<VerticalRecyclerViewFastScroller> weakFastScroller;
    private final WeakReference<SectionTitleIndicator> weakSectionTitleIndicator;
    private final WeakReference<RecyclerView> weakRecyclerView;

    public FastScrollWrapper(RecyclerView recyclerView, VerticalRecyclerViewFastScroller fastScroller,
            SectionTitleIndicator sectionTitleIndicator) {
        weakFastScroller = new WeakReference<>(fastScroller);
        weakSectionTitleIndicator = new WeakReference<>(sectionTitleIndicator);
        weakRecyclerView = new WeakReference<>(recyclerView);
        //this.fastScrollWrapper = fastScrollWrapper;

        fastScroller.setRecyclerView(recyclerView);
        recyclerView.setOnScrollListener(fastScroller.getOnScrollListener());
        fastScroller.setSectionIndicator(sectionTitleIndicator);
        hideFastScroller(0);
    }

    public void determineSetFastScroll(final int totItems) {
        RecyclerView recyclerView = weakRecyclerView.get();
        if (recyclerView == null) return;

        // only show fast scroll if the number of items is > MIN_ITEMS_SHOW
        if (totItems > MIN_ITEMS_SHOW) {
            recyclerView.removeOnScrollListener(this);
            recyclerView.addOnScrollListener(this);
        } else {
            recyclerView.removeOnScrollListener(this);
        }
    }

    private void showFastScroller() {
        VerticalRecyclerViewFastScroller fastScroller = weakFastScroller.get();
        SectionTitleIndicator sectionTitleIndicator = weakSectionTitleIndicator.get();
        if (fastScroller == null || sectionTitleIndicator == null) return;

        mHandler.removeCallbacksAndMessages(null);
        fastScroller.setVisibility(View.VISIBLE);
        sectionTitleIndicator.setVisibility(View.VISIBLE);
        hideFastScroller(TIMEOUT_SCROLL_BAR);
    }

    private void hideFastScroller(final int timeOffset) {
        VerticalRecyclerViewFastScroller fastScroller = weakFastScroller.get();
        SectionTitleIndicator sectionTitleIndicator = weakSectionTitleIndicator.get();
        if (fastScroller == null || sectionTitleIndicator == null) return;

        mHandler.postDelayed(()-> {
            fastScroller.setVisibility(View.GONE);
            sectionTitleIndicator.setVisibility(View.GONE);
        }, timeOffset);
    }

    @Override
    public void onScrollStateChanged(final RecyclerView recyclerView, final int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if(newState == RecyclerView.SCROLL_STATE_DRAGGING) {
            //showFastScroller();
        } else {
            hideFastScroller(TIMEOUT_SCROLL_BAR);
        }
    }

    @Override
    public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
        super.onScrolled(recyclerView, dx, dy);
        showFastScroller();
    }
}
