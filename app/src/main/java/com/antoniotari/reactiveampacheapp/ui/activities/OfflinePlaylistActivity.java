package com.antoniotari.reactiveampacheapp.ui.activities;

import android.app.Activity;
import android.content.Intent;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;

/**
 * Created by antoniotari on 2017-04-20.
 */

public class OfflinePlaylistActivity extends PlaylistActivity {

    @Override
    protected void loadSongs(Playlist playlist) {
        initSongList(((LocalPlaylist)playlist).getSongList());
    }

    @Override
    protected boolean isLocalPlaylist() {
        return false;
    }

    @Override
    protected void onRefresh() {
        swipeLayout.setRefreshing(false);
    }

    public static Intent createIntent(Activity activity) {
        LocalPlaylist playlist = PlaylistManager.INSTANCE.getOfflinePlaylist();
        Intent intent = new Intent(activity, OfflinePlaylistActivity.class);
        intent.putExtra(PlaylistActivity.KEY_INTENT_PLAYLIST, playlist);
        return intent;
    }
}
