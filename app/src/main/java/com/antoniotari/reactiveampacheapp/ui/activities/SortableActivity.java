package com.antoniotari.reactiveampacheapp.ui.activities;

import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.reactiveampache.models.Sortable;
import com.antoniotari.reactiveampache.models.Sortable.SortOption;
import com.antoniotari.reactiveampache.utils.AmpacheUtils;
import com.antoniotari.reactiveampacheapp.R;

/**
 * Created by antoniotari on 2017-05-21.
 */

public abstract class SortableActivity extends BaseDetailActivity {

    protected abstract SortOption getDefaultSortOption();
    protected abstract List<? extends Sortable> getSortableList();
    protected abstract SortOption[] getSortOptions();
    protected abstract RecyclerView.Adapter getAdapter();
    protected abstract String getSortTagName();

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_submenuSort);
        SubMenu subMenu = menuItem.getSubMenu();
        for (SortOption sortOption:getSortOptions()) {
            final String title = sortOption != SortOption.TAG ? sortOption.toString() : getSortTagName();
            subMenu.add(Menu.NONE, sortOption.getId(), Menu.NONE, title);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        List<? extends Sortable> sortableList = getSortableList();
        if (sortableList != null) {

            int id = item.getItemId();
            //String title = String.valueOf(item.getTitle());

            for (SortOption sortOption : SortOption.values()) {
                if (sortOption.getId() == id) {
                    AmpacheUtils.sort(sortableList, sortOption);
                    getAdapter().notifyDataSetChanged();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(),R.string.sort_not_ready,Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
