package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.widget.SectionIndexer;

import java.util.List;

import com.antoniotari.reactiveampache.models.AmpacheModel;

/**
 * Created by antoniotari on 2017-03-13.
 */

abstract class SectionIndexerAdapter<T extends AmpacheModel, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> implements SectionIndexer {

    protected abstract List<T> getItems();

    @Override
    public Object[] getSections() {
        return getItems().toArray();
    }

    @Override
    public int getPositionForSection(final int sectionIndex) {
        return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
        if (position >= getItems().size()) {
            position = getItems().size() - 1;
        }
        AmpacheModel ampacheModel = getItems().get(position);
        return getItems().indexOf(ampacheModel);
    }
}
