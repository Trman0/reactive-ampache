/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import com.antoniotari.reactiveampacheapp.ui.fragments.BaseFragment;

/**
 * Created by antonio tari on 2016-05-21.
 */
public class TabsAdapter extends FragmentStatePagerAdapter {

    private TabsAdapterEntry[] mEntries;
    private final Map<Integer,BaseFragment> mPageReferenceMap = new HashMap<>();


    public TabsAdapter(FragmentManager fm, TabsAdapterEntry... entries) {
        super(fm);
        if (entries == null || entries.length == 0) {
            throw new RuntimeException("must provide entries");
        }

        mEntries = entries;
    }

    @Override
    public Fragment getItem(int position) {
        try {
            BaseFragment fragment = mEntries[position].fragmentClass.newInstance();
            mPageReferenceMap.put(position, fragment);
            return fragment;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseFragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

    @Override
    public int getCount() {
        return mEntries.length;
    }

    public TabsAdapterEntry[] getEntries() {
        return mEntries;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mEntries[position].title;
    }

    public void destroyItem (ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }

    public static class TabsAdapterEntry {
        private String title;
        private Class<? extends BaseFragment> fragmentClass;

        public TabsAdapterEntry(final String title, final Class<? extends BaseFragment> fragmentClass) {
            this.title = title;
            this.fragmentClass = fragmentClass;
        }

        public Class<? extends BaseFragment> getFragmentClass() {
            return fragmentClass;
        }
    }
}