/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp;

import android.app.Application;

import java.util.Collections;

import com.antoniotari.android.lastfm.LastFm;
import com.antoniotari.audiosister.AudioSister;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.managers.PreferencesManager;
import com.crashlytics.android.Crashlytics;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;

/**
 * Created by antonio tari on 2016-05-24.
 */
public class AmpApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PlaylistManager.INSTANCE.init(this);
        PreferencesManager.INSTANCE.init(this);
        PlayManager.INSTANCE.init(this);
        // init last fm and ampache library
        String lastFmApiKey = getString(R.string.lastfm_api_key_default);//TextUtils.isEmpty(BuildConfig.LASTFM_API_KEY) ? getString(R.string.lastfm_api_key_default) : BuildConfig.LASTFM_API_KEY;
        LastFm.INSTANCE.init(lastFmApiKey);
        AmpacheApi.INSTANCE.initSession(this);
        inittPicasso();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AudioSister.getInstance().kill();
    }

    private void inittPicasso(){
        final OkHttpClient client = new OkHttpClient.Builder()
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .build();
        final Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(client))
                .build();
        Picasso.setSingletonInstance(picasso);
    }
}
